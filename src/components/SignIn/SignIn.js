import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Api from "../../api/Api";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 0),
    },
}));

export default function SignIn({onAuth, setPage, page}) {
    const classes = useStyles();
    const [address, setAddress] = useState("localhost:8000")
    const [name, setName] = useState("")
    const [password, setPassword] = useState("")
    const [invalidPassword, setInvalidPassword] = useState(false)

    const onSignIn = () => {
        Api.auth(address, name, password).then(data => {
            localStorage.setItem("address", address);
            localStorage.setItem("session_id", data.session_id);
            localStorage.setItem("user", JSON.stringify(data.user));

            onAuth(data.session_id, data.user, address);
        }).catch(error => {
            setInvalidPassword(true);
        })
    }

    if(!page){
        return (
            <form className={classes.form} noValidate>
                <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    label="Server address"
                    autoFocus
                    value = {address}
                    onChange={e => setAddress(e.target.value)}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    label="Name"
                    value = {name}
                    onChange={e => {setName(e.target.value); setInvalidPassword(false)}}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    label="Password"
                    type="password"
                    value = {password}
                    onChange={e => {setPassword(e.target.value);setInvalidPassword(false)}}
                />
                <p style={{display: invalidPassword? 'inline':'none'}}>Неверный пароль или имя пользователя.</p>
                <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={onSignIn}
                >
                    Войти
                </Button>
                <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={()=>{
                        setPage(true)
                    }}
                >
                    Зарегистрироваться
                </Button>
            </form>
        );
    }
    else return (
        <div>

        </div>
    )

}