import React, {useState} from 'react';
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Api from "../../api/Api";

export default function ChatAddUserDialog({open, onClose, chatId}) {
  const [userName, setUserName] = useState('');

  const addUserInChat = () =>{
    Api.addUserInChat(chatId, userName);
    onClose();
  }

  return (
      <Dialog open={open} onClose={onClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Добавление друга</DialogTitle>
        <DialogContent>
          <TextField
              autoFocus
              margin="dense"
              label="Имя пользователя"
              fullWidth
              value={userName}
              onChange={e => setUserName(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="secondary">
            Отмена
          </Button>
          <Button onClick={addUserInChat} color="primary">
            Добавить
          </Button>
        </DialogActions>
      </Dialog>
  );
}