import React, {useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Api from "../../api/Api";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function SignUp({page, setPage}) {
    const classes = useStyles();
    const [name, setName] = useState("")
    const [password, setPassword] = useState("")
    const [invalidName, setInvalidName] = useState(false)

    const onSignUp = () => {
        Api.checkIn(name, password).catch(error => {
            setInvalidName(true);
        })
    }

    if(page){
        return (
            <form className={classes.form} noValidate>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            label="Nickname"
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            variant="outlined"
                            required
                            fullWidth
                            label="Password"
                            type="password"
                            value={password}
                            autoComplete="current-password"
                            onChange={e => setPassword(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <p style={{display: invalidName? 'inline':'none'}}>Пользователь с таким именем уже существует.</p>
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={onSignUp}
                >
                    Зарегистрироваться
                </Button>
                <Grid container justify="flex-end">
                    <Grid item>
                        <Link href="#" variant="body2" onClick={()=>{setPage(false)}}>
                            Уже есть аккаунт? Войти
                        </Link>
                    </Grid>
                </Grid>
            </form>
        );
    }
    else return (
        <div>

        </div>
    )

}