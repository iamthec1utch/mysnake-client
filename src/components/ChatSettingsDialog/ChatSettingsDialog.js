import React, {useState} from 'react';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Tooltip
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Api from "../../api/Api";
import Avatar from "@material-ui/core/Avatar";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import List from "@material-ui/core/List";
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import ClearIcon from '@material-ui/icons/Clear';
import ChatAddUserDialog from "../ChatAddUserDialog/ChatAddUserDialog";

let meAdmin = false;

export default function ChatSettingsDialog({chatId, chatName, chatBio, setIsAdmin, isAdmin, usersChat, open, onClose}) {
  const [bio, setBio] = useState(chatBio);
  const [name, setName] = useState(chatName);
  const [fileName, setFileName] = useState('');
  const fileInput = React.useRef(null);
  const [openDialog, setOpenDialog] = useState(false);

  const onSave = () => {
    Api.editChat(chatId, name, bio);
    onClose();
  }

  const leaveFromChat = (userId) => {
    console.log("leave", chatId, userId);
    Api.leaveFromChat(chatId, userId);
  }

  const setAdminChat = (userId) =>{
    console.log("set admin", chatId, userId);
    let isAdminCopy = [...isAdmin];
    for(let i=0; i<isAdminCopy.length; i++){
      if(isAdminCopy[i].userId === userId){
        isAdminCopy[i].isAdmin = true;
      }
    }
    setIsAdmin(isAdminCopy);
    console.log(isAdminCopy);
    Api.setAdminChat(chatId,userId);
  }

  const deleteAdminChat = (userId) =>{
    if(meAdmin){
      console.log("delete admin", chatId, userId);
    }
  }

  const onSelectAvatar = (e) => {
    console.log(e.target.files[0]);
    Api.uploadChatAvatar(e.target.files[0], chatId).then(() => {
      setFileName(e.target.files[0].name)
    })
  }

  return (
      <Dialog open={open} onClose={onClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Настройки чата</DialogTitle>
        <DialogContent>
          <div style={{display: "flex", alignItems:"flex-start" }}>
            <Avatar style={{'margin-right': "15px"}}
                    src={Api.avatarChat(chatId, fileName)}
                    onClick={() => fileInput.current.click()} />
            <input style={{display: 'none'}} ref={fileInput} type="file" name="file" onChange={onSelectAvatar}/>
            <TextField
                autoFocus
                margin="dense"
                label="Chat name"
                fullWidth
                value={name.name}
                defaultValue={chatName}
                onChange={e => { if(meAdmin) setName(e.target.value)}}
            />
          </div>
          <TextField
              autoFocus
              margin="dense"
              label="Information"
              fullWidth
              value={bio}
              defaultValue={chatBio}
              onChange={e => {if(meAdmin) setBio(e.target.value)}}
          />

          <Button fullWidth onClick={e => setOpenDialog(true)}>Добавить друга</Button>
          <List dense>
            {usersChat?.map((user) => {
              return (
                  <div>
                    <ListItem key={user.id}>
                      <ListItemAvatar>
                        <Avatar
                            alt={user.name}
                            src={Api.avatar(user.id)}
                        />
                      </ListItemAvatar>
                      <Tooltip title={user.bio}>
                        <ListItemText primary={user.name} />
                      </Tooltip>
                      <ListItemSecondaryAction>
                        {isAdmin?.map((value) => {
                          if(value.userId === user.id){
                            if(value.isAdmin){
                              if(localStorage.getItem("user").includes('"id":'+value.userId)){
                                meAdmin=true;
                              }
                              return (
                                  <div>
                                    <StarIcon color="primary" onClick={() => deleteAdminChat(value.userId)} style={{ fontSize: 20}}/>
                                    <ClearIcon color="primary" onClick={() => leaveFromChat(value.userId)} style={{ fontSize: 20, display:meAdmin ? 'inline':'none', 'margin-left':'10px'}}/>
                                  </div>
                              );
                            }
                            else{
                              return (
                                  <div>
                                    <StarBorderIcon color="primary" onClick={() => setAdminChat(value.userId)} style={{ fontSize: 20, display:meAdmin ? 'inline':'none'}}/>
                                    <ClearIcon color="primary" onClick={() => leaveFromChat(value.userId)} style={{ fontSize: 20, display:meAdmin||localStorage.getItem("user").includes('"id":'+value.userId) ? 'inline':'none', 'margin-left':'10px'}}/>
                                  </div>
                              );
                            }

                          }
                        })}
                      </ListItemSecondaryAction>
                    </ListItem>
                  </div>
              );
            })}
          </List>
          <ChatAddUserDialog open={openDialog} onClose={e => setOpenDialog(false)} chatId={chatId}/>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="secondary">
            Отмена
          </Button>
          <Button onClick={onSave} color="primary">
            Сохранить
          </Button>
        </DialogActions>
      </Dialog>
  );
}