import React from 'react';

import DoneIcon from '@material-ui/icons/Done';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import TimerIcon from '@material-ui/icons/Timer';

export default function StateMessage({isMe, wasRead, wasSent}) {

    if (isMe && wasSent){
        return (
            <div>
                <TimerIcon style={{right : "0px"}} />
            </div>
        )
    }
    else if(isMe && !wasRead){
        return (
            <div>
                <DoneIcon style={{right : "0px"}} />
            </div>
        )
    }
    else if(isMe && wasRead){
        return (
            <div>
                <DoneAllIcon style={{right : "0px"}}/>
            </div>
        )
    }
    else{
        return (
            <div>

            </div>
        )
    }
}