import React, {useEffect, useState} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import {Button, Tooltip} from "@material-ui/core";
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import Api from "../../api/Api";

let chatIdFromResp;
let chatNameFromResp;
export default function UserList({users, chatId, onSelectChat}) {

    const onCreateChat = (user) =>{
        if(JSON.parse(localStorage.getItem("user")).id !== user.id){
            Api.newChat(user.name, user.name, true).then(data=>{
                if (!Number.isInteger(data)){
                    data?.map((chat) => {
                        console.log(chat.isPrivateDialog);
                        chatIdFromResp = chat.id;
                        chatNameFromResp = chat.name;
                        onSelectChat(chat.id, chat.name, chat.bio, chat.isPrivateDialog)
                    });
                }

            })
        }

    }
    return (
        <div>
            <List dense>
                {users?.map((user) => {
                    return (
                        <div>
                            <ListItem selected = {chatIdFromResp===chatId && chatNameFromResp===user.name} key={user.id} button onClick={() => onCreateChat(user)}>
                                <ListItemAvatar>
                                    <Avatar
                                        alt={user.name}
                                        src={Api.avatar(user.id)}
                                    />
                                </ListItemAvatar>
                                <Tooltip title={user.bio == null? '': user.bio}>
                                    <ListItemText primary={user.name} />
                                </Tooltip>
                                <ListItemSecondaryAction>
                                    <FiberManualRecordIcon fontSize="inherit"  style={{color: user.online ? 'green' : 'red'}}/>
                                </ListItemSecondaryAction>
                            </ListItem>
                        </div>
                    );
                })}
            </List>
        </div>
    );
}