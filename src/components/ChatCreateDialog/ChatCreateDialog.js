import React, {useState} from 'react';
import {Dialog, DialogActions, DialogContent, DialogTitle, TextField, Tooltip} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Api from "../../api/Api";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import Chip from "@material-ui/core/Chip";

let avatarFile = null;
export default function ChatCreateDialog({open, onClose, users}) {
  const [name, setName] = useState('');
  const [userNames, setUserNames] = useState('');
  const [currentUser, setCurrentUser] = useState('');
  const [chooseUsers, setChooseUsers] = useState([]);

  const [fileName, setFileName] = useState('');
  const fileInput = React.useRef(null);

  const onCreate = () => {
    chooseUsers.forEach((user)=>{
      setUserNames(userNames + ", " + user.name);
    })
    Api.newChat(name, userNames, false).then(data => {
      Api.uploadChatAvatar(avatarFile, data).then(() => {
        setFileName(avatarFile.name)
      })
      onClose();
      setUserNames('');
    })
  }

  const onSelectAvatar = (e) => {
    avatarFile = e.target.files[0];
  }

  const searchUser = (e) =>{
    setCurrentUser(e.target.value);
  }

  const addUser = (user) =>{
    if(chooseUsers.indexOf(user)<0){
      let cache = [...chooseUsers, user];
      setChooseUsers(cache);
    }
    else{
      let cache = [...chooseUsers];
      cache.splice(chooseUsers.indexOf(user),1);
      setChooseUsers(cache)
    }
  }

  return (
      <Dialog open={open} onClose={onClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Создание чата</DialogTitle>
        <DialogContent>
          <TextField
              autoFocus
              margin="dense"
              label="Название"
              fullWidth
              value={name}
              onChange={e => setName(e.target.value)}
          />
          <TextField
              autoFocus
              margin="dense"
              label="Имена пользователей"
              fullWidth
              value={currentUser}
              onChange={searchUser}
          />
          {chooseUsers?.map(user => (
              <Chip
                  avatar={<Avatar alt={user.name} src={Api.avatar(user.id)}/>}
                  label={user.name}
                  onClick={()=>{}}
              />
          ))}
          {users.filter(person => person.name.toLowerCase().search(currentUser.toLowerCase())!== -1 && JSON.parse(localStorage.getItem("user")).id !== person.id).map(filteredPerson => (
              <div>
                <ListItem key={filteredPerson.id} button onClick={()=>{addUser(filteredPerson);}}>
                  <ListItemAvatar>
                    <Avatar
                        alt={filteredPerson.name}
                        src={Api.avatar(filteredPerson.id)}
                    />
                  </ListItemAvatar>
                  <Tooltip title={filteredPerson.bio == null? '': filteredPerson.bio}>
                    <ListItemText primary={filteredPerson.name} />
                  </Tooltip>
                </ListItem>
              </div>
          ))}

          <Button style={{ margin: '5px 35% 0px' }} variant="contained" color="primary" component="span"
                  onClick={() => {
                    fileInput.current.click()
                  }}>
            Upload avatar
          </Button>
          <input style={{display: 'none'}} ref={fileInput} type="file" name="file" onChange={onSelectAvatar}/>

        </DialogContent>
        <DialogActions>
          <Button onClick={()=>{ onClose(); setUserNames('');}} color="secondary">
            Отмена
          </Button>
          <Button onClick={onCreate} color="primary">
            Создать
          </Button>
        </DialogActions>
      </Dialog>
  );
}