import React, {useState} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import {Button} from "@material-ui/core";
import Api from "../../api/Api";
import ChatCreateDialog from "../ChatCreateDialog/ChatCreateDialog";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";

export default function ChatList({chats, onSelectChat, chatId, users}) {
    const [openDialog, setOpenDialog] = useState(false);
    return (
        <div>
            <Button fullWidth onClick={e => setOpenDialog(true)}>Создать чат</Button>
            <List dense>
                {chats?.map((chat) => {
                    if(!chat.isPrivateDialog)
                        return (
                            <ListItem selected = {chatId === chat.id} key={chat.id} button onClick={() => onSelectChat(chat.id, chat.name, chat.bio, chat.isPrivateDialog)}>
                                <ListItemAvatar>
                                    <Avatar
                                        alt={chat.name}
                                        src={Api.avatarChat(chat.id)}
                                    />
                                </ListItemAvatar>
                                <ListItemText primary={chat.name} />
                            </ListItem>
                        );
                })}
            </List>
            <ChatCreateDialog open={openDialog} onClose={e => setOpenDialog(false)} users={users}/>
        </div>
    );
}