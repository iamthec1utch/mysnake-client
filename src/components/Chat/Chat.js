import React, {useState} from 'react';
import Message from "../Message/Message";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import classes from "./Chat.module.css"
import Api from "../../api/Api";
import Button from "@material-ui/core/Button";
import ChatSettingsDialog from "../ChatSettingsDialog/ChatSettingsDialog";
import ListItem from "@material-ui/core/ListItem";
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import PhotoIcon from '@material-ui/icons/Photo';
import YouTubeIcon from '@material-ui/icons/YouTube';
import AudiotrackIcon from '@material-ui/icons/Audiotrack';
import CloseIcon from '@material-ui/icons/Close';
import CreateIcon from '@material-ui/icons/Create';
import ClearIcon from '@material-ui/icons/Clear';
import VisibilitySensor from 'react-visibility-sensor';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';

let attach;
let messageId;
let messageBody;
let currentUserIsAdmin;
let isEditing = false;
let currentText = "";

export default function CheckboxListSecondary({messages, chatId, chatName, chatBio, chatIsPrivate, user, userMap}) {
    const [messageText, setMessageText] = useState('')
    const [openDialog, setOpenDialog] = useState(false);
    const [usersChat, setUsersChat] = useState([]);
    const [isAdmin, setIsAdmin] = useState([]);
    const [type, setType] = useState([]);
    const [wasSent, setWasSent] = useState(false);
    const [fileName, setFileName] = useState('');
    const fileInput_image = React.useRef(null);
    const fileInput_video = React.useRef(null);
    const fileInput_audio = React.useRef(null);
    const fileInput_another = React.useRef(null);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [thisMessage, setThisMessage] = React.useState(null);

    const [wasRead, setWasRead] = React.useState(null);
    const currentUser = JSON.parse(localStorage.getItem("user"))

    const onKeyPressed = (e) =>{
        if (e.keyCode === 13){
            sendMsg();
        }
    }
    const sendMsg = () =>{
        currentText = messageText;
        if (isEditing === false){
            setWasSent(true);
            Api.sendMessage(chatId, messageText, type,  attach).then(()=> {
                let div = document.getElementById("chat");
                div.scrollTop = div.scrollHeight;
                setWasSent(false);
            });
        }
        else{
            Api.editMessage(messageId, messageText, type,  attach);
        }
        setMessageText("");
        clearAttach();
    }
    const clearAttach = () =>{
        setFileName("");
        attach = null;
        setType("");
    }

    const uploadFile = (e) =>{
        setFileName(e.target.files[0].name)
        attach = e.target.files[0];
        if(e.target.name === "another"){
            let str = e.target.files[0].name.toString();
            let result = str.match(/(\.[^\s@]*)/);
            setType(result[0]);
        }
        else
            setType(e.target.name);
    }
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const messageClick = (userId) => {
        Api.getIsAdmin(currentUser.id, chatId).then((data)=>{

            if (userId === currentUser.id){ // свое сообщение
                currentUserIsAdmin = false;
                setThisMessage(true);
            }
            else if(userId !== currentUser.id && data){ // админ
                currentUserIsAdmin = true;
                setThisMessage(true);
            }
            else if(userId !== currentUser.id && !data){ // ничего
                setThisMessage(false);
            }
        })

    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    const messageClose = () => {
        setThisMessage(null);
    };
    const deleteMessage = () => {
        Api.deleteMessage(messageId, chatId);
    }
    const editMessage = () => {
        setMessageText(messageBody);
        isEditing = true;
        setFileName(attach);
    }

    return (
        <Paper className={classes.ChatContainer}>
            <div>
                <ListItem button fullWidth selected={true} style={{color: "black"}}>{chatName}
                    <Button style={{display: chatIsPrivate? 'none':'inline', 'margin-left':'73%'}}
                            onClick={(e) =>
                            { setOpenDialog(true);
                                Api.getUsersChat(chatId).then(data => {
                                    setUsersChat(data.usersInChat);
                                    setIsAdmin(data.is_admin);
                                })}}
                    >Информация</Button>
                    <ChatSettingsDialog chatId={chatId} chatName={chatName} chatBio={chatBio} setIsAdmin={setIsAdmin} isAdmin={isAdmin} usersChat={usersChat} open={openDialog} onClose={e => setOpenDialog(false)}/>
                </ListItem>

            </div>
            <Paper id="chat" style={{ 'overflow-y': "auto", height:fileName==='' ? '85%': '80%' }} className={classes.ChatContainer}>
                <div>
                    {messages?.map((message) => {
                        if(chatId===message?.chatId){
                            return (
                                <VisibilitySensor
                                    onChange={(isVisible) => {
                                        setWasRead({visibility: isVisible})
                                        if (!message?.wasRead && wasRead && message?.userId !== currentUser.id) {
                                            Api.readMessage(message.id);
                                        }
                                    }}
                                >
                                    <div>

                                        <div aria-controls="menu" aria-haspopup="true" onClick={() => {
                                            messageId = message.id;
                                            messageBody = message.text;
                                            attach = message?.fileInMessage;
                                            messageClick(message.userId)
                                        }}>
                                            <Message name={userMap[message.userId]?.name} text={message?.text}
                                                     isMe={user.id === message.userId} fileInMessage={message?.fileInMessage}
                                                     type={message?.type} isEdited={message?.isEdited}
                                                     wasRead={message?.wasRead}/>
                                        </div>
                                        <Menu style={{marginLeft: '73.5%'}}
                                            id="menu"
                                            anchorEl={thisMessage}
                                            keepMounted
                                            open={Boolean(thisMessage)}
                                            onClose={messageClose}
                                        >
                                            <MenuItem onClick={() => {
                                                deleteMessage();
                                                messageClose();
                                            }}><CloseIcon/> Удалить</MenuItem>
                                            <MenuItem style={{display: currentUserIsAdmin ? "none" : "inline"}} onClick={() => {
                                                editMessage();
                                                messageClose();
                                            }}><CreateIcon/> Редактировать</MenuItem>
                                        </Menu>

                                    </div>
                                </VisibilitySensor>
                            )
                        }
                        }
                    )}
                    <div style={{display: wasSent === true ? 'inline':'none'}}>
                        <Message name={currentUser.name} text={currentText}
                                 isMe={true} fileInMessage={attach}
                                 type={null} isEdited={false} wasSent={wasSent}/>
                    </div>
                </div>

            </Paper>
            <div style={{display:chatId!==null ? 'inline':'none'}} className={classes.ChatInput}>
                <CloudUploadIcon aria-controls="simple-menu" aria-haspopup="true" className={classes.FileInput} onClick={ handleClick}/>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                    <MenuItem onClick={() => {fileInput_image.current.click(); handleClose()}}><PhotoIcon/>  Фото</MenuItem>
                    <input style={{display: 'none'}} ref={fileInput_image} type="file" name="image" accept=".img,.png,.jpeg,.jpg" onChange={uploadFile}/>
                    <MenuItem onClick={() => {fileInput_video.current.click(); handleClose()}}><YouTubeIcon/>  Видео</MenuItem>
                    <input style={{display: 'none'}} ref={fileInput_video} type="file" name="video" accept=".mp4,.mkv" onChange={uploadFile}/>
                    <MenuItem onClick={() => {fileInput_audio.current.click(); handleClose()}}><AudiotrackIcon/>  Аудио</MenuItem>
                    <input style={{display: 'none'}} ref={fileInput_audio} type="file" name="audio" accept=".mp3,.mpeg,.ogg" onChange={uploadFile}/>
                    <MenuItem onClick={() => {fileInput_another.current.click(); handleClose()}}><InsertDriveFileIcon/>  Файл</MenuItem>
                    <input style={{display: 'none'}} ref={fileInput_another} type="file" name="another" accept=".pdf,.txt,.rar,.docx,.pptx" onChange={uploadFile}/>
                </Menu>
                <TextField className={classes.ChatTextField}
                           onKeyDown={onKeyPressed}
                           tabIndex="0"
                           value={messageText}
                           onChange={e => setMessageText(e.target.value)}
                           multiline
                           rows={1}
                           defaultValue=""
                           variant="outlined"
                />
                <Button
                    className={classes.button}
                    onClick={sendMsg}
                >Отправить</Button>
                <div style={{display:fileName!=='' ? 'inline':'none'}}>
                    <p style={{margin: '7px', display: 'inline'}}>Прикрепленный файл: {fileName}</p>
                    <ClearIcon style={{display: 'inline', 'font-size':'15px','margin':'0px'}} onClick={clearAttach}/>
                </div>
            </div>
        </Paper>
    );
}