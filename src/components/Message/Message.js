import React, {useState} from 'react';
import classes from "./Message.module.css"
import Media from "../Media/Media";
import Api from "../../api/Api";
import StateMessage from "../StateMessage/StateMessage";

let link = null;
export default function Message({send, name, text, isMe, fileInMessage, type, isEdited, wasRead, wasSent}) {

    const [typeFromMes, setTypeFromMes] = useState('');

    function httpHtml(content) {
        const reg = /(http:\/\/|https:\/\/)((\w|=|\?|\.|\/|&|-)+)[^\s@]*/g;

        let textWithLink = content.replace(reg, "<a target='_blank' href='$1$2'>$1$2</a>");
        link = (content.match(reg));
        if (link != null)
        {
            Api.linkInMessage(link).then(data => {
                const contentType = data.headers.get('content-type');
                if (contentType.includes('image/jpeg') || contentType.includes('image/*')) {
                    setTypeFromMes("image");
                }
                else if (contentType.includes('video/mp4') || contentType.includes('video/ogg')) {
                    setTypeFromMes("video");
                }
                else if (contentType.includes('audio/mp3') || contentType.includes('audio/mpeg') || contentType.includes('audio/ogg')) {
                    setTypeFromMes("audio");
                }
            });
        }
        return textWithLink;
    }
    if((type === null || type === undefined) && typeFromMes === ''){
        return (
            <div className={isMe ? classes.MessageRight : classes.MessageLeft}>
                <div className={classes.MessageContent}>
                    <div className={classes.Name}>
                        {name}
                    </div>
                    <div className={classes.Text}>
                        <div style={{'word-break': 'break-all'}} dangerouslySetInnerHTML={{
                            __html: httpHtml(text)
                        }} />
                    </div>
                    <div>
                        <p style = {{'font-size':'11px', float:'right', margin:'0px','margin-right':'3px', display: !isEdited? "none": "inline"}}>(ред.)</p>
                        <StateMessage isMe={isMe} wasRead={wasRead} wasSent={wasSent}/>
                    </div>
                </div>
            </div>
        );
    }
    else if((type === null || type === undefined) && typeFromMes !== ''){
        return (
            <div className={isMe ? classes.MessageRight : classes.MessageLeft}>
                <div className={classes.MessageContent}>
                    <div className={classes.Name}>
                        {name}
                    </div>
                    <div className={classes.Text}>
                        <div style={{'word-break': 'break-all'}} dangerouslySetInnerHTML={{
                            __html: httpHtml(text)
                        }} />
                    </div>
                    <div className="container">
                        {console.log("Параметры", link, typeFromMes)}
                        <Media fileInMessage={link} type={typeFromMes} isLink={true}/>
                    </div>
                    <div>
                        <p style = {{display: !isEdited? "none": "inline"}}>(ред.)</p>
                        <StateMessage isMe={isMe} wasRead={wasRead} wasSent={wasSent}/>
                    </div>
                </div>
            </div>
        );
    }
    else {
        return (
            <div className={isMe ? classes.MessageRight : classes.MessageLeft}>
                <div className={classes.MessageContent}>
                    <div className={classes.Name}>
                        {name}
                    </div>
                    <div className={classes.Text}>
                        <div style={{'word-break': 'break-all'}} dangerouslySetInnerHTML={{
                            __html: httpHtml(text)
                        }} />
                    </div>
                    <div className="container">
                        <Media fileInMessage={fileInMessage} type={type} isLink={false}/>
                    </div>
                    <div>
                        <p style = {{display: !isEdited? "none": "inline"}}>(ред.)</p>
                        <StateMessage isMe={isMe} wasRead={wasRead} wasSent={wasSent}/>
                    </div>
                </div>
            </div>
        );
    }
}