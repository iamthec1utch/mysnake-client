import React, {useState} from 'react';
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, TextField} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Api from "../../api/Api";


export default function UserProfileEditDialog({user, setUser, open, onClose}) {
  const [name, setName] = useState(user?.name);
  const [bio, setBio] = useState(user?.bio);

  const onSave = () => {
    Api.profile(name, bio).then(data => {
      let newUser = Object.assign({}, user);
      newUser.name = name
      newUser.bio = bio

      setUser(newUser)
      localStorage.setItem("user", JSON.stringify(newUser))

      onClose();
    })
  }

  return (
    <Dialog open={open} onClose={onClose} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Редактирование профиля</DialogTitle>
      <DialogContent>
        <TextField
          autoFocus
          margin="dense"
          label="Name"
          fullWidth
          value={name}
          onChange={e => setName(e.target.value)}
        />
        <TextField
          autoFocus
          margin="dense"
          label="Bio"
          fullWidth
          value={bio}
          onChange={e => setBio(e.target.value)}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="secondary">
          Отмена
        </Button>
        <Button onClick={onSave} color="primary">
          Сохранить
        </Button>
      </DialogActions>
    </Dialog>
  );
}