import React from 'react';
import Api from "../../api/Api";
import AudioPlayer from "../AudioPlayer";
import {ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import InsertDriveFileIcon from "@material-ui/icons/InsertDriveFile";

let src;
export default function Media({fileInMessage, type, isLink}) {

    const audioRef = React.useRef(null);
    if(isLink){
        src = fileInMessage;
    }
    else{
        src = Api.fileInMessage(fileInMessage);
    }
    if(type === "image"){
        return (
            <div>
                <img width="100%" height="100%" src={src}/>
            </div>
        )}
    else if (type === "video"){
        return (
            <div>
                <video width="100%" height="100%" controls src={src}/>
            </div>
        )}
    else if (type === "audio"){
        return (
            <div>
                <AudioPlayer width="100%" height="100%" ref={audioRef} src={src}/>
            </div>
        )}
    else{
        return (
            <div>
                <ListItem button >
                    <ListItemIcon>
                        <InsertDriveFileIcon />
                    </ListItemIcon>
                    <ListItemText primary={src}/>
                </ListItem>
            </div>
        )}
}