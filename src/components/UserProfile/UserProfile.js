import React, {useState} from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import UserProfileEditDialog from "../UserProfileEditDialog/UserProfileEditDialog";
import Api from "../../api/Api";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import {Button} from "@material-ui/core";

export default function UserProfile({user, setUser}) {
  const [openDialog, setOpenDialog] = useState(false);
  const [fileName, setFileName] = useState('');

  const fileInput = React.useRef(null);


  const onSelectAvatar = (e) => {
    Api.uploadAvatar(e.target.files[0]).then(() => {
      setFileName(e.target.files[0].name)
    })
  }

  return (
    <div>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt={user?.name} src={Api.avatar(user?.id, fileName)} onClick={() => fileInput.current.click()} />
          <input style={{display: 'none'}} ref={fileInput} type="file" name="file" onChange={onSelectAvatar}/>
        </ListItemAvatar>
        <ListItemText
          primary={user?.name}
          secondary={user?.bio}
          onClick={e => setOpenDialog(true)}
        />
        <Button style={{marginTop: '10px', backgroundColor: '#7FFF00'}}
            variant="contained"
            startIcon={<ExitToAppIcon />}
            onClick={()=>{
              localStorage.clear();
              window.location.reload();
            }}
        >
        </Button>
      </ListItem>
      <UserProfileEditDialog user={user} setUser={setUser} open={openDialog} onClose={e => setOpenDialog(false)}/>
    </div>
  );
}

//Api.uploadAvatar(file)