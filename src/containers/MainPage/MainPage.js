import React, {useState} from 'react';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Chat from "../../components/Chat/Chat";
import Paper from "@material-ui/core/Paper";
import UserList from "../../components/UserList/UserList";
import UserProfile from "../../components/UserProfile/UserProfile";
import ChatList from "../../components/ChatList/ChatList";
import Api from "../../api/Api";
import BackgroundImage from "../../background.png"

export default function MainPage({user, users, setUser, chats, messages, userMap, onMessages}) {

    const [chatId, setChatId] = useState(null);
    const [chatName, setChatName] = useState('');
    const [chatBio, setChatBio] = useState(null);
    const [chatIsPrivate, setChatIsPrivate] = useState(null);

    const onSelectChat = (selectChatId, selectChatName, selectChatBio, selectChatIsPrivate) =>{
        console.log("ChatId "+chatId);
        setChatName(selectChatName);
        setChatId(selectChatId);
        setChatBio(selectChatBio);
        setChatIsPrivate(selectChatIsPrivate)
        Api.getHistory(selectChatId).then(data => {
            console.log(data)
            onMessages(data)
            let div = document.getElementById("chat");
            div.scrollTop = div.scrollHeight;
        });
    }

    return (
      <Container component="main" maxWidth="md">
          <Grid container spacing={3}>
              <Grid item xs={4}>
                  <Paper style={{margin: '10px 0'}}>
                      <UserProfile user={user} setUser={setUser}/>
                  </Paper>
                  <Paper style={{margin: '10px 0'}}>
                      <ChatList chats={chats} onSelectChat={onSelectChat} chatId={chatId} users={users}/>
                      <UserList users={users} chatId={chatId} onSelectChat={onSelectChat}/>
                  </Paper>
              </Grid>
              <Grid item xs={8}>
                  <Paper style={{margin: '10px 0'}}>
                      <Chat messages={messages} chatId={chatId} chatName={chatName} chatBio={chatBio} chatIsPrivate={chatIsPrivate} user={user} userMap={userMap}/>
                  </Paper>
              </Grid>
          </Grid>
      </Container>
    );
}