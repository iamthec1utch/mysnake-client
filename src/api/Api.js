let url = null
class Api {

  /**
   * Метод отправляет на сервер данные о пользователе и возвращает сессию и пользователя
   * @param address aдрес сервера
   * @param name имя пользователя
   * @param password пароль пользователя
   * @returns HashMap<String, Object>
   *          ("session_id", значение);
   *          ("user", значение);
   */

  static auth(address, name, password) {
    Api.setAddress(address)

    return fetch('http://' + address + '/auth', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({name: name, password: password})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод проверяет существует ли пользователь с введенным именем и паролем
   * @param name имя пользователя при попытке входа
   * @param password пароль пользователя при попытке входа
   */

  static checkIn(name, password) {
    return fetch('http://localhost:8000/check-in', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({name: name, password: password})
    }).then((response) => {
      return response.json();
    })
  }


  static avatar(userID, fileName) {
    return url + "/avatar?userId=" + userID + "&fileName=" + fileName
  }


  /**
   * Метод получения ссылки на аватарку чата
   * @param userId индентификатор пользователя
   * @param fileName имя файла
   * @returns {string} ссылка на аватарку
   */
  static avatar(userId, fileName) {
    return url + "/avatar?userId=" + userId + "&fileName=" + fileName
  }

  /**
   * Метод получения ссылки на аватарку чата
   * @param chatID айди чата
   * @param fileName имя файла
   * @returns {string} ссылка на аватарку
   */

  static avatarChat(chatID, fileName) {
    return url + "/avatar-chat?chatId=" + chatID + "&fileName=" + fileName
  }


  /**
   * Метод получения ссылки на файл
   * @param fileAddress ссылка на файл
   * @returns {string} ссылка на файл
   */

  static fileInMessage(fileAddress) {
    return url + "/" +fileAddress
  }

  /**
   *
   * Метод загрузки файлов из ссылки
   * @param link ссылка
   */
  static linkInMessage(link) {
    return fetch(link, {
      method: 'GET',
      mode: 'cors',
      headers: {
      },
    }).then((response) => {
      return response;
    })
  }

  /**

   * Метод отвечет за заполнения профиля пользователя
   * @param name имя пользователя
   * @param bio описание пользователя

   */
  static profile(name, bio) {
    return fetch(url + '/profile', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({name: name, bio: bio})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод вызывается при редактировании чата
   * @param id айди чата
   * @param name новое имя чата
   * @param bio новое описание чата
   */

  static editChat(id, name, bio) {
    return fetch(url + '/edit-chat', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({id:id, name: name, bio: bio})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод вызывается при создании нового чата
   * @param name название чата
   * @param usersNames строка с именами пользователей, которые будут добавлены в чат
   * @param isPrivate является чат личным чатом
   * @returns {Promise<Response>}
   */

  static newChat(name, usersNames, isPrivate) {
    return fetch(url + '/new-chat', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({name: name, usersNames: usersNames, isPrivate:isPrivate})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод вызывается при выходе/кике из чата
   * @param chatId айди чата
   * @param userId айди пользователя
   */

  static leaveFromChat(chatId, userId) {
    return fetch(url + '/leave-from-chat', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({chatId: chatId, userId: userId})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод отвечает за удаление сообщения
   * @param messageId айди сообщения
   * @param chatId айди чата
   */

  static deleteMessage(messageId, chatId) {
    return fetch(url + '/delete-message', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({messageId: messageId, chatId:chatId})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод вызывается при прочтении сообщения
   * @param messageId айди сообщения
   */

  static readMessage(messageId) {
    return fetch(url + '/read-message', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({messageId: messageId})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод отвечает за редактирование сообщения
   * @param messageId айди сообщения
   * @param messageText отредактированный текст сообщения
   * @param type тип прикрепления
   * @param attach прикрепление
   */

  static editMessage(messageId, messageText, type,  attach) {
    let formData = new FormData();
    formData.append("attach", attach);

    return fetch(url + '/edit-message?messageId=' + messageId + '&messageText=' + messageText + '&type=' + type, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'SessionId': localStorage.getItem("session_id")
      },
      body: formData
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод проверяет является ли выбранный пользователь админом чата
   * @param userId айди пользователя
   * @param chatId айди чата
   * @returns Boolean является админом или нет
   */

  static getIsAdmin(userId, chatId) {
    return fetch(url + '/get-is-admin', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({userId: userId, chatId:chatId})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод выдает права на администрирование в чате
   * @param chatId айди чата
   * @param userId айди пользователя, котору дали права
   */

  static setAdminChat(chatId, userId) {
    return fetch(url + '/set-admin-chat', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({chatId: chatId, userId: userId})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод добавления пользователя в чат
   * @param chatId айди чата
   * @param userName имя пользователя
   */

  static addUserInChat(chatId, userName) {
    return fetch(url + '/add-to-chat', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({chatId: chatId, userName: userName})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод получает всех пользователей чата и список админов
   * @param chatId идентификатор чата
   * @returns HashMap<String, Object>;
   *          ("usersInChat", значение); список пользователей
   *          ("is_admin", значение); список айди пользователя и значения админ он или нет
   */

  static getUsersChat(chatId) {
    return fetch(url + '/get-users-chat', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({chatId: chatId})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод подгружает историю сообщений конкретного чата
   * @param chatId индентификатор чата
   * @returns List<Message> список сообщений
   */

  static getHistory(chatId) {
    return fetch(url + '/get-history', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({chatId: chatId})
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод отправляет сообщение в чат
   * @param chatId чат, в который отправляется сообщение
   * @param message текст сообщения
   * @param type тип прикрепления
   * @param attach прикрепленный файл
   */

  static sendMessage(chatId, message, type, attach) {
    let formData = new FormData();
    formData.append("attach", attach);

    return fetch(url + '/send-message?chatId=' + chatId + '&message=' + message + '&type=' + type, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'SessionId': localStorage.getItem("session_id")
      },
      body: formData
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод сообщает что пользователь активный по сессии пользователя
   */

  static isOnline() {
    return fetch(url + '/online', {
      method: 'GET',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод получает список всех чатов пользователя по сессии пользователя
   * @returns {Promise<Response>} List<Chat> список чатов пользователя
   */

  static getChats() {
    return fetch(url + '/get-chats', {
      method: 'GET',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод получает список всех пользователей приложения
   * @returns {Promise<Response>} List<User> список пользователей
   */

  static getUsers() {
    return fetch(url + '/get-users', {
      method: 'GET',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
    }).then((response) => {
      return response.json();
    })
  }


  /**
   *
   * @param file
   * @returns {Promise<Response>}
   */

  static uploadAvatar(file) {
    let formData = new FormData();
    formData.append("avatar", file);

    return fetch(url + '/avatar', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'SessionId': localStorage.getItem("session_id")
      },
      body: formData
    }).then((response) => {
      return response.json();
    })
  }


  /**
   * Метод загружает картинку чата
   * @param file картинка
   * @param chatId индентификатор чата
   * @returns {Promise<Response>}
   */

  static uploadChatAvatar(file, chatId) {
    let formData = new FormData();
    formData.append("avatar", file);

    return fetch(url + '/avatar-chat?chatId='+chatId, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'SessionId': localStorage.getItem("session_id")
      },
      body: formData
    }).then((response) => {
      return response.json();
    })
  }

  static chatIsCreated(userId) {
    return fetch(url + '/chat-is-created', {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        'SessionId': localStorage.getItem("session_id")
      },
      body: JSON.stringify({userId: userId})
    }).then((response) => {
      return response.json();
    })
  }



  /**
   * Метод задает адрес сервера
   * @param address адрес сервера
   */

  static setAddress(address) {
    url = 'http://' + address
  }
}

export default Api