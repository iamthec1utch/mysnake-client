import React, {useEffect, useState} from 'react';
import AuthPage from "./containers/AuthPage/AuthPage";
import MainPage from "./containers/MainPage/MainPage";
import Api from "./api/Api";
import BackgroundImage from "../src/background.png";

let cacheMessages = []

export default function App() {
  const [isAuth, setIsAuth] = useState(false)
  const [user, setUser] = useState(null)
  const [users, setUsers] = useState([])
  const [userMap, setUserMap] = useState({})
  const [chats, setChats] = useState([])
  const [messages, setMessages] = useState([
      //{userId: 8, text: "Привет!", wasRead:true},
      //{userId: 9, text: "Ты попал в самый крутой чат - MySnake!", wasRead:true},
      //{userId: 8, text: "Общайся с друзьями!", wasRead:true},
      //{userId: 9, text: "Обменивайся фото...", type: "image", fileInMessage: "file-in-message?messageId=48&type=image", wasRead:true},
      //{userId: 8, text: "...видео и аудио!", type: "video", fileInMessage: "file-in-message?messageId=28&type=video", wasRead:true},
      //{userId: 9, text: "Выбери человека, с которым хочешь пообщаться, либо создай чат!", wasRead:true},
  ])

  useEffect(() => {
    const sessionId = localStorage.getItem("session_id")
    if (sessionId) {
      const address = localStorage.getItem("address")
      const user = JSON.parse(localStorage.getItem("user"))
      auth(sessionId, user, address)
    }
  },[])

  const auth = (sessionId, user, address) => {
    setUser(user)
    setIsAuth(true)
    Api.setAddress(address)

    let eventSource = new EventSource('http://' + address + '/sse?sessionId=' + sessionId)
    eventSource.onopen = () => {
      Api.getUsers().then(data => {
        setUsers(data)
        const uMap = {}
        data.forEach(u => {
          uMap[u.id] = u
        })
        setUserMap(uMap)
      })
      Api.getChats().then(data => {
        setChats(data)
        console.log(chats);
      })
      Api.isOnline();
    }
    eventSource.onmessage = onMessage;

    // надо как то поправить
    eventSource.addEventListener("newChat", message => {
      console.log(message.data);
      Api.getChats().then(data => {
        setChats(data)
      });
      //setChats([...chats, JSON.parse(message.data)]);
    });

    eventSource.addEventListener("deleteMessage", message => {
      onMessages(JSON.parse(message.data));
    });

    // надо как то поправить
    eventSource.addEventListener("newUser", message => {
      console.log(message.data);
      Api.getUsers().then(data => {
        setUsers(data)
        const uMap = {}
        data.forEach(u => {
          uMap[u.id] = u
        })
        setUserMap(uMap)
      })
    });

  }
  const onMessage = (message) => {
    console.log(message);
    cacheMessages = [...cacheMessages, JSON.parse(message.data)];
    setMessages(cacheMessages);
  }
  const onMessages = (messages) =>{
    setMessages(messages)
    cacheMessages = messages
  }


  return (
      <div style={{backgroundImage: `url(${BackgroundImage})` }}>
        {isAuth && <MainPage user={user} users={users} setUser={setUser} chats={chats} messages={messages} userMap={userMap} onMessages={onMessages}/>}
        {!isAuth && <AuthPage onAuth={auth}/>}
      </div>
  );
}